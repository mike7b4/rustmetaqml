use qmetaobject::*;
use url::{Url, form_urlencoded::Parse};
use std;
use std::ffi::OsStr;
use std::collections::LinkedList;
use std::net::TcpListener;
use std::thread;
use std::net::TcpStream;
use std::io::{Read,Write};
use config::{ServerConfig};
use std::fs::{self, DirEntry};
use std::path::{Path,PathBuf};
use std::fs::File;
use std::collections::HashMap;
#[derive(Default, QObject)]
pub struct Server {
    base: qt_base_class!{trait QObject},
    status: qt_property!(String; READ get_status WRITE set_status NOTIFY status_changed),
    status_changed: qt_signal!(),
    address: qt_property!(String; READ get_address WRITE set_address  NOTIFY address_changed),
    address_changed: qt_signal!(),

    activate: qt_method!(fn(&mut self) -> bool),
    init: qt_method!(fn(&mut self) -> bool),
}

struct MediaHandler {
}

#[derive(PartialEq, Eq)]
pub enum MediaType {
    NotSet,
    Image,
}

pub struct MediaData {
    pub url: String,
    pub name: String,
    pub next: String,
    pub mtype: MediaType,
}

impl MediaData {
    fn new() -> Self {
        MediaData{
            url: String::from(""),
            name: String::from(""),
            next: String::from(""),
            mtype: MediaType::NotSet
        }
    }
}

impl MediaHandler {
    pub fn new() -> Self {
        MediaHandler{ }
    }

    pub fn respond(&mut self, client: &mut Client, mut items: LinkedList<&str>, indir: &Path, map: HashMap<String,String>) {
        let mut dir = PathBuf::from(indir);
        let mut subdir = String::from("");
        let mut cmd = String::from("list");
        println!("cmd  {:?}", &items);
        if items.is_empty() == false {
            cmd = String::from(items.pop_front().unwrap());
        }
        for item in items {
            dir.push(&item);
            subdir += &String::from(format!("/{}", item));
        }

        match cmd.as_ref() {
            "list" => {
                let list = self.list_dir(client, &dir, &subdir);
                self.respond_list_html(client, &subdir, list);
            },
            "image" => self.image(client, dir),
            "show" => self.show(client, subdir, map.get("next").unwrap_or(&String::new()).as_ref()),
            "gallery" => self.gallery(client, &subdir),
            "list.json" => {
                let list = self.list_dir(client, &dir, &subdir);
                self.respond_json(client, list);
            },
            _ => client.send_error(404, "Not found")
        };
    }

    fn respond_json(&mut self, client: &mut Client, list: LinkedList<MediaData>) {
        client.send_ok_with_attributes(format!("Content-Type: text/json\r\n"));
        let mut js = json!({});
        for current in list {
            js[current.name] = json!(current.url);
        }
        client.send_string(js.to_string());
    }

    fn list_dir(&mut self, client: &mut Client, dir: &PathBuf, subdir: &str) -> LinkedList<MediaData>{
        let mut vec: LinkedList<MediaData> = LinkedList::new();
        if dir.is_dir() {
            println!("Read dir {:?}", dir);
            if let Ok(entries) = fs::read_dir(&dir) {
                let mut entries: Vec<DirEntry> = entries.map(|r| r.unwrap()).collect::<Vec<_>>();
                entries.sort_by_key(|entries| entries.path());
                while let Some(entry) = entries.pop() {
                        let path = entry.path();
                        let mut item = MediaData::new();
                        let mut fname = path.file_name().unwrap_or(OsStr::new("")).to_str().unwrap_or("");
                        item.name = String::from(fname);
                        item.next = String::from("");
                        if path.is_dir() {
                            item.url = String::from(format!("/media/gallery/{}", subdir));
                        } else {
                            if item.name.ends_with(".jpg") || item.name.ends_with(".JPG") {
                                item.url = String::from(format!("/media/image/{}", subdir));
                                item.mtype = MediaType::Image;
                            } else {
                                continue;
                            }
                        }
                        vec.push_front(item);
                }
            } else {
                println!("dir {:?}", &dir);
                client.send_error(404, "Not found");
            }
        } else {
            // readfile...
            client.send_error(400, "Bad request");
        }
        vec
    }

    fn respond_list_html(&mut self, client: &mut Client, subdir: &str, list: LinkedList<MediaData>)
    {
        client.send_ok();
        client.send_head();
        client.send_body_start();
        let mut body = String::new();
        body += &format!("<a href=\"{}\"><button>Back</button></a>\n", subdir);
        body += &format!("<table>");
        for current in list {
            body += &format!("<tr>");
            body += &format!("<td><a href=\"{}/{}\">{}</a><br /></td>\n", current.url, current.name, current.name);
            body += &format!("</tr>");
            if current.mtype == MediaType::Image {
                body += &format!("<img src=\"/media/image/{}/{}\" />", current.url.replace("/media/show/", ""), current.name);
            }
        }
        body += &format!("</tr>");
        client.send_body_end();
        client.send_string(body);
    }

    fn image(&mut self, client: &mut Client, file: PathBuf) {
        client.send_file(file);
    }

    fn gallery(&mut self, client: &mut Client, subdir: &str) {
        client.send_template(PathBuf::from("template/image_page.html"), &subdir);
    }

    fn show(&mut self, client: &mut Client, file: String, next: &str) {
        client.send_ok();
        client.send_head();
        client.send_body_start();
        println!("is: {}", file);
        let nexturl = PathBuf::from(&file);
        let nexturl = nexturl.parent().unwrap_or(Path::new(""));
        let nexturl = nexturl.join(next).to_str().unwrap_or(&String::from("")).to_string();
        client.send_string(format!("<a href=\"/media/show/{}\"><button>Next</button></a>\n", nexturl));
        client.send_string(format!("<img src=\"/media/image/{}\" />", file));
        client.send_string(format!("<a href=\"/media/show/{}\"><button>Next</button></a>\n", nexturl));
        client.send_body_end();
    }
}

pub struct Client {
    stream: TcpStream,
}

impl Client {
    pub fn send_ok(&mut self) {
        match self.stream.write(b"HTTP/1.1 200 OK\r\n\r\n") {
            Ok(_) => {},
            Err(_) => println!("Failed write"),
        }
    }
    pub fn send_ok_with_attributes(&mut self, attr: String) {
        match self.stream.write(format!("HTTP/1.1 200 OK\r\n{}\r\n", attr).into_bytes().as_slice()) {
            Ok(_) => {},
            Err(e) => println!("{}", e),
        }
    }

    pub fn send_error(&mut self, code: u16, text: &str)
    {
        match self.stream.write(format!("HTTP/1.1 {} OK\r\n\r\n{}\r\n", code, text).into_bytes().as_slice()) {
            Ok(_) => {},
            Err(e) => println!("{}", e),
        }
    }

    pub fn send_head(&mut self) {
        self.send_string(String::from("<html><head><link href=\"/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\" /></head>"));
    }

    pub fn send_body_start(&mut self) {
        self.send_bytes(b"<body>".to_vec());
    }

    pub fn send_body_end(&mut self) {
        self.send_bytes(b"</body></html>".to_vec());
    }

    pub fn send_string(&mut self, data: String) {
        self.send_bytes(data.into_bytes());
    }

    pub fn send_bytes(&mut self, data: Vec<u8>) {
        match self.stream.write(data.as_slice()) {
            Ok(_) => {},
            Err(e) => println!("{}", e),
        }
    }

    fn html_from_template(&mut self, buf: Vec<u8>, subdir: &str) {
        let mut buf = String::from(String::from_utf8_lossy(&buf));
        buf = buf.replace("%IMAGE_DIR%", subdir);
        let buf = buf.as_bytes().to_vec();
        self.send_ok_with_attributes(format!("Content-Length: {}\r\n", buf.len()));
        self.send_bytes(buf);
    }
    pub fn send_template(&mut self, file: PathBuf, subdir: &str) {
        let mut file = match File::open(&file) {
            Ok(file) => {
                file
            },
            Err(_) => {
                self.send_error(404, "Not found");
                return ;
            }
        };
        let mut buf = Vec::new();
        match file.read_to_end(&mut buf) {
            Ok(_) => {
                self.html_from_template(buf, &subdir);
            },
            Err(_) => {
                self.send_error(404, "Not found")
            }
        };
    }

    pub fn send_file(&mut self, file: PathBuf) {
        if file.is_file() {
            let mut buf = Vec::new();
            let mut file = match File::open(&file) {
                Ok(file) => {
                    file
                },
                Err(_) => {
                    self.send_error(404, "Not found");
                    return;
                }
            };
            match file.read_to_end(&mut buf) {
                Ok(size) => {
                    self.send_ok_with_attributes(format!("Content-Length: {}\r\n", size));
                    self.send_bytes(buf);
                },
                Err(_) => self.send_error(404, "Not found"),
            }
        } else {
            self.send_error(404, "Not found");
        }
    }

    fn handle_media(&mut self, items: LinkedList<&str>, querys: Parse) {
        let config = ServerConfig::new();
        let mut media = MediaHandler::new();
        let map: HashMap<_, _> = querys.into_owned().collect();
        println!("{:?}", map);
        media.respond(self, items, Path::new(&config.pictures), map);
    }

    fn handle_function(&mut self, mut items: LinkedList<&str>, url: Url) {
        let function = items.pop_front().unwrap_or("");
        match function {
            "media" => self.handle_media(items, url.query_pairs()),
            "favicon.ico" => self.send_file(PathBuf::from("favicon.ico")),
            "stylesheet.css" => self.send_file(PathBuf::from("stylesheet.css")),
            _ => self.send_error(404, "Not found..."),
        };
    }

    fn handle_url(&mut self, path: &str) {
        let url = Url::parse(format!("http://localhost{}", path).as_str()).unwrap();
        let segments = url.path_segments().unwrap();
        let mut items: LinkedList<&str> = LinkedList::new();
        for item in segments {
            items.push_back(item);
        }
        if items.len() < 1 {
            self.send_error(404, "Not found.");
        } else {
            self.handle_function(items, Url::from(url.clone()));
        }
    }

    fn handle_client(&mut self) {
        let mut inbuf = [0;4096];
        match self.stream.read(&mut inbuf) {
            Ok(_) => {
                let inbuf = String::from_utf8_lossy(&inbuf);
                let inbuf = inbuf.lines().next().unwrap_or("");
                if inbuf.starts_with("GET ") && inbuf.ends_with(" HTTP/1.1") {
                    let url = inbuf[4..].split(" HTTP/1.1").next().unwrap();
                    println!("url {}", url);
                    self.handle_url(url);
                } else {
                    println!("Bad request {}", inbuf);
                    self.send_error(400, "Bad request");
                }
            },
            Err(_) => println!("Client did not send any data?"),
        }; 
    }
}

impl Server {
    fn init(&mut self) -> bool {
        let config = ServerConfig::new();
        self.set_status("Rust server is down.".to_string());
        self.set_address(config.host);
        true
    }
    fn get_status(&self) -> String {
        self.status.to_string()
    }
    fn set_status(&mut self, status: String) {
        self.status = status;
        println!("{}", self.status);
        self.status_changed();
    }
    fn get_address(&self) -> String {
        self.address.to_string()
    }
    fn set_address(&mut self, address: String) {
        self.address = address;
        self.address_changed();
    }

    fn activate(&mut self) -> bool {
        println!("{}", self.address);
        match TcpListener::bind(&self.address) {
            Ok(listener) => {
                self.set_status("Rust server is up.".to_string());
                thread::spawn(move || {
                    for stream in listener.incoming() {
                        match stream {
                            Ok(stream) => client_connection(stream),
                            Err(e) => println!("Could not get client {:?}", e), //self.set_status(format!("Could not get client\n{}", e)),
                        };
                    }
                    fn client_connection(stream: TcpStream) {
                        let mut client = Client{stream: stream};//({stream: stream});
                        client.handle_client();
                        //println!("{:?}", stream);
                    }
                });
            },
            Err(e) => self.set_status(e.to_string()),
        };
        true
    }

}

