extern crate url;
extern crate qmetaobject;
#[macro_use]
extern crate serde_json;
use qmetaobject::*;
use std::ffi::CStr;
mod implementation;
mod config;
fn main() {
    qml_register_type::<implementation::Server>(CStr::from_bytes_with_nul(b"RustTest\0").unwrap(), 1, 0, CStr::from_bytes_with_nul(b"Server\0").unwrap());
    let mut engine = QmlEngine::new();
    engine.load_file("main.qml".into());
    engine.exec();
}
