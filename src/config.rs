extern crate config;
use std::collections::HashMap;

pub struct ServerConfig {
    pub host: String,
    pub pictures: String,
}
impl Default for ServerConfig {
    fn default() -> ServerConfig {
        ServerConfig {
            host: "".to_string(),
            pictures: "".to_string(),
        }
    }
}

impl ServerConfig {
     pub fn new() -> ServerConfig {
        let mut settings = config::Config::default();
        let mut config = ServerConfig::default();
        settings
            .merge(config::File::with_name("/home/mikael/.config/librem-webserver/settings")).expect("settings.toml file not found");
        let map = settings.try_into::<HashMap<String, String>>().unwrap_or(HashMap::new());
        for (key, value) in map {
            match key.as_ref() {
                "host" => config.host = value,
                "pictures" => config.pictures = value,
                _ =>  {},
            };
        }
        config
    }
}
