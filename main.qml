import QtQuick 2
import QtQuick.Controls 1
import RustTest 1.0

ApplicationWindow
{
    id: window
    opacity: 10
    visible: true
    height: 800
    width: 600
    title: server.status
    Server {
        id: server
    }

    Column {
        anchors.fill: parent
        TextField {
            id: entryServerIP
            font.pointSize: 32
            text: server.address
            placeholderText: "Give an IP:PORT"
            inputMask: "000.000.000.000:0000;"
            height: 60
            width: parent.width
            onAccepted: {
                server.address = text;
                server.activate()
            }
        }
        Text {
            text: server.status
            font.pointSize: 32
            width: parent.width
            height: 60
        }
    }
    Component.onCompleted: {
        server.init()
        server.activate()
    }
}
